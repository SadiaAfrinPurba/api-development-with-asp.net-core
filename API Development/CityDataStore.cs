﻿using API_Development.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Development
{
    public class CityDataStore
    {
        //this attribute makes sure that we get the same data as long as we do not restart
        public static CityDataStore Current { get; } = new CityDataStore ();
        public List<CityDTO> Cities { get; set; }
        public CityDataStore()
        {
            Cities = new List<CityDTO>()
            {
                 new CityDTO()
                 {
                     Id = 1,
                     Name = "Dhaka",
                     Description = "The capital city of Bangladesh.",
                     PointOfInterest = new List<PointOfInterestDTO>()
                     {
                         new PointOfInterestDTO()
                         {
                             Id = 1,
                             Name = "Malibag",
                             Description = "Nice area to live."
                         },
                         new PointOfInterestDTO()
                         {
                             Id = 2,
                             Name = "Mirpur",
                             Description = "Crowded area."
                         }
                     }
                 },
                 new CityDTO()
                 {
                    Id = 2,
                    Name = "Cumilla",
                    Description = "Beautiful city.",
                    PointOfInterest = new List<PointOfInterestDTO>()
                    {
                        new PointOfInterestDTO
                        {
                            Id = 1,
                            Name = "Pointofinterest_1",
                            Description = "Some descrition_1"
                        }
                    }
                 },
                 new CityDTO()
                 {
                   Id = 3,
                   Name = "Kurigram",
                   Description = "Another beautiful city.",
                   PointOfInterest = new List<PointOfInterestDTO>()
                   {
                       new PointOfInterestDTO
                       {
                           Id = 1,
                           Name = "Some name 1",
                           Description = "Some description 1"
                       },
                       new PointOfInterestDTO
                       {
                           Id = 2,
                           Name = "Some name 2",
                           Description = "Some description 2"
                       },
                       new PointOfInterestDTO
                       {
                           Id = 3,
                           Name = "Some name 3",
                           Description = "Some description 3"
                       }

                   }
                 }

            };
        }
    }
}
